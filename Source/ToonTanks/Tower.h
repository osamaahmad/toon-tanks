// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tower.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API ATower : public ABasePawn
{
	GENERATED_BODY()

protected:

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

	class ATank* Enemy = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat") float FireRange = 1000;
	UPROPERTY(EditAnywhere, Category = "Combat") float RotationSpeed = 40;
	UPROPERTY(EditAnywhere, Category = "Combat") float FirePeriod = 2;

private:

	void UpdateInfo();

	FVector MyLocation;
	FVector EnemyLocation;
	bool EnemyIsInRange;
	FTimerHandle TimerHandle;

public:

	void RotateTowardsEnemy(float DeltaTime);
	virtual AProjectile* Fire() override;
};
