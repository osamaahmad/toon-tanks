// Fill out your copyright notice in the Description page of Project Settings.


#include "ToonTanksGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "BasePawn.h"
#include "ToonTanksPlayerController.h"
#include "Tower.h"


void AToonTanksGameMode::ActorDied(ABasePawn* Actor)
{
	Actor->HandleDestruction();
	if (Actor == MainPlayer) {
		Controller->SetPlayerEnabled(false);
		GameOver(false);
	} else {
		TargetTowersCount--;
		if (TargetTowersCount == 0)
			GameOver(true);
	}
}

void AToonTanksGameMode::BeginPlay()
{
	Super::BeginPlay();
	InitMemberVariables();
	HandleGameStart();
	SetTargetTowersCount();
}

void AToonTanksGameMode::InitMemberVariables()
{
	MainPlayer = Cast<ABasePawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MainPlayer == nullptr)
		UE_LOG(LogTemp, Error, TEXT("Couldn't get hold of the main player. This might lead to the game getting crashed."))

	Controller = Cast<AToonTanksPlayerController>(MainPlayer->GetController());
	if (Controller == nullptr)
		UE_LOG(LogTemp, Error, TEXT("Couldn't get hold of the main player controller. This might lead to the game getting crashed."))
}

void AToonTanksGameMode::HandleGameStart()
{
	StartGame();
	Controller->SetPlayerEnabled(false);
	auto Delegate = FTimerDelegate::CreateUObject(Controller, &AToonTanksPlayerController::SetPlayerEnabled, true);
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, Delegate, StartDelay, false);
}

float AToonTanksGameMode::GetStartDelay() const 
{ 
	return StartDelay;
}

void AToonTanksGameMode::SetTargetTowersCount()
{
	TArray<AActor*> Towers;
	UGameplayStatics::GetAllActorsOfClass(this, ATower::StaticClass(), Towers);
	TargetTowersCount = Towers.Num();
}