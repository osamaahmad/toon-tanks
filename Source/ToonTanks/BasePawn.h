// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BasePawn.generated.h"

class AProjectile;

UCLASS()
class TOONTANKS_API ABasePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABasePawn();

	virtual AProjectile* Fire();
	virtual void HandleDestruction();

protected:

	virtual void PlayDeathSoundEffects();
	virtual void ShowDeathVisualEffects();
	virtual void Die();

	// This is just a variant of the Fire() function that returns void.
	// This function has to return void to be able to be bound with an action.
	void _Fire();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Combat")
		class TSubclassOf<UCameraShakeBase> CameraShakeType = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat")
		class USoundBase* ExplosionSound = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat")
		class UParticleSystem* DeathParticles = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = true)) 
		class UCapsuleComponent* Capsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = true)) 
		UStaticMeshComponent* BaseMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = true)) 
		UStaticMeshComponent* TurretMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = true)) 
		USceneComponent* ProjectileSpawnPoint;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile", meta = (AllowPrivateAccess = true)) 
		TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile", meta = (AllowPrivateAccess = true))
		float ProjectileForce = 2000;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = true)) 
	class UHealthComponent* Health;
};
