// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Tank.h"
#include "Kismet/GameplayStatics.h"
#include "Tank.h"

void ATower::BeginPlay()
{
	Super::BeginPlay();
	Enemy = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATower::_Fire, FirePeriod, true);
}

void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateInfo();
	RotateTowardsEnemy(DeltaTime);
}

void ATower::UpdateInfo()
{
	if (!Enemy)
		return;

	EnemyLocation = Enemy->GetActorLocation();
	MyLocation = GetActorLocation();

	EnemyIsInRange = FVector::Distance(MyLocation, EnemyLocation) <= FireRange;
	EnemyIsInRange &= !Enemy->IsDead();
}

void ATower::RotateTowardsEnemy(float DeltaTime)
{
	if (!EnemyIsInRange)
		return;

	double CurrentYaw = TurretMesh->GetComponentRotation().Yaw;
	double TargetYaw = (EnemyLocation - MyLocation).Rotation().Yaw;

	double RotationAmount = FMath::Abs(TargetYaw - CurrentYaw);
	if (RotationAmount > 180)
		TargetYaw = 360 * -FMath::Sign(TargetYaw);

	double YawComponent = FMath::FInterpConstantTo(CurrentYaw, TargetYaw, DeltaTime, RotationSpeed);

	TurretMesh->SetWorldRotation({ 0, YawComponent, 0 });
}

AProjectile* ATower::Fire()
{
	if (!EnemyIsInRange)
		return nullptr;
	return Super::Fire();
}
