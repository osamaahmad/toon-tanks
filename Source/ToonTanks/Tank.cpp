// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Projectile.h"

ATank::ATank()
{
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Sping Arm Component"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	CameraComponent->SetupAttachment(SpringArmComponent);
}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATank::MoveFarward);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ATank::Turn);
	PlayerInputComponent->BindAxis(TEXT("RotateTurret"), this, &ATank::RotateTurret);
	PlayerInputComponent->BindAxis(TEXT("ToggleTurretRotateWithCamera"), this, &ATank::ToggleTurretRotateWithCamera);
	PlayerInputComponent->BindAxis(TEXT("RotateCameraX"), this, &ATank::RotateCameraX);
	PlayerInputComponent->BindAxis(TEXT("RotateCameraY"), this, &ATank::RotateCameraY);
	PlayerInputComponent->BindAxis(TEXT("MoveCamera"), this, &ATank::MoveCamera);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ATank::_Fire);
}

// Called every frame
void ATank::Tick(float _DeltaTime)
{
	Super::Tick(_DeltaTime);
	this->DeltaTime = _DeltaTime;
}

void ATank::MoveFarward(float Direction)
{
	float Delta = MovementSpeed * Direction * DeltaTime;
	AddActorLocalOffset({ Delta, 0, 0 }, true);
}

void ATank::Turn(float Direction)
{
	float Delta = TurningSpeed * Direction * DeltaTime;
	AddActorLocalRotation({ 0, Delta, 0 }, true);
}

void ATank::RotateTurret(float Value)
{
	float Delta = CameraRotationSpeed * Value * DeltaTime;
	TurretMesh->AddLocalRotation({ 0, Delta, 0 });
}

void ATank::ToggleTurretRotateWithCamera(float Value)
{
	if (Value) {
		float Time = UGameplayStatics::GetRealTimeSeconds(this);
		if (Time - LastToggleTime >= MinToggleDifference)
			TurretRotateWithCamera = !TurretRotateWithCamera;
		LastToggleTime = Time;
	}
}

void ATank::RotateCameraX(float Value)
{
	if (TurretRotateWithCamera)
		RotateTurret(Value);
	float Delta = CameraRotationSpeed * Value * DeltaTime;
	SpringArmComponent->AddWorldRotation({ 0, Delta, 0 }); // World Rotation
}

void ATank::RotateCameraY(float Value)
{
	float Delta = CameraRotationSpeed * Value * DeltaTime;
	SpringArmComponent->AddLocalRotation({ Delta, 0, 0 }); // Local Rotation
}

void ATank::MoveCamera(float Direction)
{
	float Delta = CameraMovementSpeed * Direction * DeltaTime;
	SpringArmComponent->TargetArmLength += Delta;
}

void ATank::Die()
{
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
	bIsDead = true;
}

bool ATank::IsDead() const { return bIsDead; }
