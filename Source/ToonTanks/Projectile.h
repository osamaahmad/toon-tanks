// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class USoundBase;

UCLASS()
class TOONTANKS_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(EditAnywhere, Category = "Combat")
		class TSubclassOf<UCameraShakeBase> CameraShakeType = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat")
		USoundBase* LaunchSound = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat")
		USoundBase* HitSound = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Combat")
		class UParticleSystemComponent* SmokeTrail = nullptr;

	UPROPERTY(EditAnywhere, Category = "Combat")
		class UParticleSystem* HitParticles = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = true))
		UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = true))
		class UProjectileMovementComponent* MovementComponent;

	UPROPERTY(EditAnywhere, Category = "Damage", meta = (AllowPrivateAccess = true))
		float Damage = 50;

	UFUNCTION()
		void OnHit(
			UPrimitiveComponent* MyHitComp,
			AActor* OtherHitActor, 
			UPrimitiveComponent* OtherHitComp, 
			FVector NormalImpulse, 
			const FHitResult& HitResult
		);
};
