// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ToonTanksGameMode.generated.h"

class ABasePawn;

/**
 *
 */
UCLASS()
class TOONTANKS_API AToonTanksGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	void ActorDied(ABasePawn* Actor);
	UFUNCTION(BlueprintCallable) float GetStartDelay() const;

protected:

	void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent) void StartGame();
	UFUNCTION(BlueprintImplementableEvent) void GameOver(bool bWonGame);

private:

	void InitMemberVariables();
	void HandleGameStart();
	void SetTargetTowersCount();

	ABasePawn* MainPlayer = nullptr;
	class AToonTanksPlayerController* Controller = nullptr;
	int32 TargetTowersCount = 0;

	UPROPERTY(EditAnywhere, Category = "Timings") float StartDelay = 3.f;
};
