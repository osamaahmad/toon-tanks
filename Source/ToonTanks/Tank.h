// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tank.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API ATank : public ABasePawn
{
	GENERATED_BODY()
	
public:

	ATank();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// Called every frame
	virtual void Tick(float _DeltaTime) override;

	void MoveFarward(float Direction);
	void RotateTurret(float Value);
	void ToggleTurretRotateWithCamera(float Value);
	void Turn(float Direction);
	void RotateCameraX(float Value);
	void RotateCameraY(float Value);
	void MoveCamera(float Direction);
	bool IsDead() const;

protected:
	
	void Die() override;

private:

	float DeltaTime;
	bool bIsDead = false;

	UPROPERTY(VisibleAnywhere, Category = "Components") class USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, Category = "Components") class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, Category = "Speed") float MovementSpeed = 500.f;
	UPROPERTY(EditAnywhere, Category = "Speed") float TurningSpeed = 200.f;

	UPROPERTY(EditAnywhere, Category = "Turret") float TurretRotationSpeed = 200.f;
	UPROPERTY(EditAnywhere, Category = "Turret") bool TurretRotateWithCamera = true;
	float LastToggleTime = 0;
	const float MinToggleDifference = 0.1;

	UPROPERTY(EditAnywhere, Category = "Camera") float CameraRotationSpeed = 200.f;
	UPROPERTY(EditAnywhere, Category = "Camera") float CameraMovementSpeed = 300.f;
};
