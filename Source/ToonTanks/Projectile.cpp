// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "Particles/ParticleSystemComponent.h"
#include "BasePawn.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	RootComponent = ProjectileMesh;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));
	MovementComponent->InitialSpeed = 2000.f;
	MovementComponent->MaxSpeed = 2000.f;
	MovementComponent->Velocity.Z = 0.05f;

	SmokeTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Smoke Trail"));
	SmokeTrail->SetupAttachment(RootComponent);

	// To avoid getting destroyed after
	// hitting its owner.
	MovementComponent->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	//FScriptDelegate Callback;
	//Callback.BindUFunction(this, FName{ "OnHit" });
	//ProjectileMesh->OnComponentHit.Add(Callback);
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	if (LaunchSound)
		UGameplayStatics::PlaySoundAtLocation(this, LaunchSound, GetActorLocation());
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UE_LOG(LogTemp, Warning, TEXT("%s"), *SmokeTrail->GetComponentLocation().ToString());
}

void AProjectile::OnHit(
	UPrimitiveComponent* MyHitComp,
	AActor* OtherHitActor, 
	UPrimitiveComponent* OtherHitComp, 
	FVector NormalImpulse, 
	const FHitResult& HitResult
)
{
	auto OwnerActor = GetOwner();
	if (OtherHitActor == OwnerActor)
		return;

	auto Controller = OwnerActor->GetInstigatorController();
	auto DamageType = UDamageType::StaticClass();
	UGameplayStatics::ApplyDamage(OtherHitActor, Damage, Controller, this, DamageType);

	if (HitParticles)
		UGameplayStatics::SpawnEmitterAtLocation(this, HitParticles, GetActorLocation(), GetActorRotation());

	if (HitSound && Cast<ABasePawn>(OtherHitActor))
		UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());

	if (CameraShakeType)
		UGameplayStatics::GetPlayerController(this, 0)->ClientStartCameraShake(CameraShakeType);

	Destroy();
}
